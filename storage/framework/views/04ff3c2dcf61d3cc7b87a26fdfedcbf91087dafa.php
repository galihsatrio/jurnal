<?php $__env->startSection('title','Tambah Data'); ?>

<?php $__env->startSection('container'); ?>
    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Tambah Data</h3>

                <?php if(count($errors)>0): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="alert alert-danger alert-sm" role="alert">
                        <?php echo e($error); ?>

                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <form method="post" action="/jurnaltambah">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                      <label for="wkt_jurnal">Waktu</label>
                      <input type="date" class="form-control" id="wkt_jurnal" placeholder="masukkan waktu" name="wkt_jurnal">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" placeholder="Masukkan keterangan" name="keterangan">
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm prepend">Selesai</button>
                    <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                </form>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Q:\Laravel\coba2\resources\views/jurnal/tambah.blade.php ENDPATH**/ ?>