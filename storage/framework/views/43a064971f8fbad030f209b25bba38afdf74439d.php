<?php $__env->startSection('title','Tambah Data'); ?>

<?php $__env->startSection('container'); ?>

    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Tambah Data</h3>

                <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <?php endif; ?>

                <div class="container">
                    <form method="post" action="/jurnaltambahitem">
                        <?php echo csrf_field(); ?>
                        <div class="form-group">
                            <label for="Jurnal_id">Keterangan</label>
                            <select name="jurnal_id" id="Jurnal_id" class="form-control" required>
                                <option value="">-  Pilih -</option>
                                <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jurnals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($jurnals->id); ?>"><?php echo e($jurnals->id); ?>. <?php echo e($jurnals->keterangan); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="nama">Nama</label>
                          <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama">
                        </div>
                        <div class="form-group">
                          <label for="saldo">Saldo</label>
                          <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo">
                        </div>

                        <button type="submit" class="btn btn-primary btn-sm prepend">Selesai</button>
                        <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Q:\Laravel\coba2\resources\views/jurnal/item/tambah.blade.php ENDPATH**/ ?>