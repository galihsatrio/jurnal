<?php $__env->startSection('title','Jurnal'); ?>

<?php $__env->startSection('search'); ?>
<a href="/jurnal" class="badge badge-sm badge-dark mr-1">Show All</a>
<form class="form-inline my-2 my-lg-0" action="/jurnal" method="get">
    <input class="form-control sm-2" type="search" placeholder="cari keterangan.." aria-label="Cari" name="search">
    <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button> <br>
    <div class="dropdown show">
        <a class="btn btn-default ml-3 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="/logout">Log Out</a>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
    <div class="container">
        <div class="row">
            <div class="col-12">

                <div class="col-6 my-4">
                    <h2>Jurnal</h2>
                </div>
                
                <?php if(count($errors)>0): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="alert alert-danger alert-sm col-12" role="alert">
                        <?php echo e($error); ?>

                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

                
                <?php if(Session::has('jurnal_success')): ?>
                <div class="alert alert-primary col-12" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo e(Session('jurnal_success')); ?>

                </div>
                <?php endif; ?>

                
                <?php if(Session::has('rekening_success')): ?>
                <div class="alert alert-primary col-12" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo e(Session('rekening_success')); ?>

                </div>
                <?php endif; ?>

                
                <?php if(Session::has('success_update_jurnal')): ?>
                <div class="alert alert-primary col-12" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo e(Session('success_update_jurnal')); ?>

                </div>
                <?php endif; ?>

                
                <?php if(Session::has('success_update_item')): ?>
                <div class="alert alert-primary col-12" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo e(Session('success_update_item')); ?>

                </div>
                <?php endif; ?>

                <div class="col-12">

                    <button type="button" class="btn btn-primary btn-sm float-right mb-2" data-toggle="modal" data-target="#TambahJurnal">
                        Add Jurnal
                    </button>

                    <table class="table table-hover table-striped table-default">
                        <thead class="thead thead-dark">
                            <tr>
                                <th width="40px">No</th>
                                <th class="text-center">Waktu</th>
                                <th class="text-center">Keterangan</th>
                                <th class="text-center" width="400px">Item</th>
                                <th class="text-center" width="150px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $no => $jurnals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($no + $jurnal->firstitem()); ?></td>
                                <td class="text-center">

                                    <p class="font-italic font-weight-light">
                                        Dibuat pada : <br>
                                        <?php echo e(\Carbon\Carbon::parse($jurnals->wkt_jurnal)->format('d, M Y')); ?><br><br><br>
                                        <?php echo e(\Carbon\Carbon::parse($jurnals->created_at)->diffForHumans()); ?>

                                    </p>

                                    
                                </td>
                                <td class="text-center"><?php echo e($jurnals->keterangan); ?></td>
                                <td>
                                    <?php if(count($items[$jurnals->id]['rekening'])): ?>
                                    <table class="table table-striped">
                                        <thead class="thead thead-dark">
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">Saldo</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $items[$jurnals->id]['rekening']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($loop->iteration); ?></td>
                                                <td><?php echo e($item->nama); ?></td>
                                                <td><?php echo e(floor($item->saldo)); ?></td>
                                                <td class="col-5">
                                                <a href="/item/<?php echo e($item->id); ?>/edit" class="btn btn-sm btn-success" >edit</a>
                                               <form class="d-inline" action="/item/<?php echo e($item->id); ?>" method="post">
                                                    <?php echo method_field('delete'); ?>
                                                    <?php echo csrf_field(); ?>
                                                   <button class="btn btn-danger btn-sm my-1 d-inline " onclick="return confirm('Apakah Anda ingin menghapusnya??')">delete</button>
                                               </form>
                                            </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td colspan="4" class="table-light border-dark">
                                                    Total Saldo  :
                                                    <div class="font-italic d-inline">
                                                        Rp.<?php echo e(floor($items[$jurnals->id]['total'])); ?> ,-

                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php else: ?>
                                        <p class="text-center font-weight-bold"><?php echo e("-- Item Kosong --"); ?></p>
                                        <p class="text-center"><?php echo e('Klik tombol "tambah item" untuk menambahkan item'); ?></p>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tambahItem">
                                        Add Item
                                    </button>
                                    <a href="/jurnal/<?php echo e($jurnals->id); ?>/edit" class="btn btn-success btn-sm my-2">edit</a>

                                    <form action="/jurnal/<?php echo e($jurnals->id); ?>" method="post">
                                        <?php echo method_field('delete'); ?>
                                        <?php echo csrf_field(); ?>
                                        <button class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda ingin menghapusnya??')">delete</button>
                                    </form>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <div class="font-weight-bold font-italic col-6">
                    Total semua saldo : <?php echo e(floor($total)); ?> <br>
                    Halaman : <?php echo e($jurnal->currentPage()); ?> <br>
                    Jumlah Data : <?php echo e($jurnal->total()); ?> <br><br>
                    <?php echo e($jurnal->links()); ?>

                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="TambahJurnal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Jurnal</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/jurnaltambah">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                      <label for="wkt_jurnal">Waktu</label>
                      <input type="date" class="form-control" id="wkt_jurnal" placeholder="masukkan waktu" name="wkt_jurnal">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" placeholder="Masukkan keterangan" name="keterangan">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kembali</button>
                    </div>
                </form>
            </div>

          </div>
        </div>
    </div>

    

  <div class="modal fade" id="tambahItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" action="/jurnaltambahitem">
                <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label for="Jurnal_id">Keterangan</label>
                    <select name="jurnal_id" id="Jurnal_id" class="form-control" required>
                        <option value="">- pilih -</option>
                        <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jurnals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($jurnals->id); ?>"><?php echo e($jurnals->id); ?>. <?php echo e($jurnals->keterangan); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama">
                </div>
                <div class="form-group">
                  <label for="saldo">Saldo</label>
                  <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                    <button type="submit" class="btn btn-secondary btn-sm" data-dismiss="modal">kembali</button>
                </div>
            </form>
        </div>

      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Q:\Laravel\coba2\resources\views/jurnal/jurnal.blade.php ENDPATH**/ ?>