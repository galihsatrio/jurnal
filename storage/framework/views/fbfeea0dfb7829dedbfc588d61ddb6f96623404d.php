<?php $__env->startSection('title','Buku Besar'); ?>

<?php $__env->startSection('search'); ?>
<a href="/bukubesar" class="badge badge-light">Show All</a>
<form class="form-inline my-2 my-lg-0" action="/bukubesar" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Cari" aria-label="Cari" name="search">
    <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('container'); ?>
    <div class="container">
        <div class="row">
            <div class="col-10">


                <h2 class="my-4">Buku Besar</h2>

                <table class="table table-hover table-striped table-light table-bordered">
                    <thead class="thead thead-dark">
                        <tr>
                            <th width="50px">No</th>
                            <th class="text-center" width="120px">Waktu</th>
                            <th class="text-center">Keterangan</th>
                            <th witdh="150px" class="text-center">Saldo</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $no => $jurnals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($no + $jurnal->firstItem()); ?></td>
                            <td class="text-center">
                                <p class="font-italic">
                                    <?php echo e(\Carbon\Carbon::parse($jurnals->wkt_jurnal)->format('d, M Y')); ?> <br>

                                </p>
                                <p class="font-italic font-weight-light">
                                    <?php echo e(\Carbon\Carbon::parse($jurnals->wkt_jurnal)->diffForHumans()); ?>

                                </p>
                            </td>
                            <td class="text-center"><?php echo e($jurnals->keterangan); ?></td>
                            <td>
                                <ul>
                                    <?php $__currentLoopData = $items[$jurnals->id]['rekening']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($item->saldo); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </td>
                            <td>
                                <?php echo e($items[$jurnals->id]['total']); ?>

                            </td>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

                    Halaman : <?php echo e($jurnal->currentPage()); ?> <br>
                    Jumlah Data : <?php echo e($jurnal->total()); ?> <br><br><br>


                <?php echo e($jurnal->links()); ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Q:\Laravel\coba2\resources\views/bukubesar/bukubesar.blade.php ENDPATH**/ ?>