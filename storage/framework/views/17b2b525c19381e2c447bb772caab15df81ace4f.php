<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Daftar</title>
  </head>
  <body class="text-center bg-light">
        <div class="container">
            <div class="row align-items-start text-center col-12">
                <?php if(Session::has('success')): ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dissmiss="alert">x</button>
                    <?php echo e(Session('success')); ?>

                </div>
                <?php endif; ?>
            </div> <br><br>
          <div class="row align-items-center">
              <div class="col-4">
                
              </div>
               <span class="border border-dark rounded-lg bg-default col-4" name="tengah" id="tengah">
                <h1 class="my-3">Daftar</h1>
                <form class="form-signin text-center md-5" method="POST" action="<?php echo e(route('register')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                      <label for="name" class="float-left">Name : </label>
                      <input type="text" class="form-control text-center border-dark <?php echo e($errors->has('name') ? 'is-invalid' : ''); ?>" id="name" name="name" value="<?php echo e(old('name')); ?>" required autofocus>
                      <?php if($errors->has('name')): ?>
                          <div class="invalid-feedback">
                            <?php echo e($errors->first('name')); ?>

                          </div>
                      <?php endif; ?>
                    </div>
                    <div class="form-group">
                      <label for="Email" class="float-left">Email :</label>
                      <input type="text" class="form-control text-center border-dark <?php echo e($errors->has('email') ? 'is-invalid' : ''); ?>" id="Email" name="email" value="<?php echo e(old('email')); ?>" required>
                      <?php if($errors->has('email')): ?>
                        <div class="invalid-feedback">
                            <?php echo e($errors->first('email')); ?>

                        </div>
                  <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label for="password" class="float-left">Password : </label>
                        <input type="password" class="form-control text-center border-dark <?php echo e($errors->has('password') ? 'is-invalid' : ''); ?>" id="password" name="password" required>
                        <?php if($errors->has('password')): ?>
                          <div class="invalid-feedback">
                            <?php echo e($errors->first('password')); ?>

                          </div>
                      <?php endif; ?>
                    </div>
                    
                    <p>Sudah punya akun? <a href="/login">Masuk</a></p>
                    <button class="btn btn-outline-dark my-3" type="submit">Daftar</button>
                </form>
               </span>
          </div>
        </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
<?php /**PATH Q:\Laravel\coba2\resources\views/login/register.blade.php ENDPATH**/ ?>