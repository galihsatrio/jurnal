<?php $__env->startSection('title','Update'); ?>


<?php $__env->startSection('container'); ?>
    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Update</h3>
                <?php if(count($errors) > 0): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="alert alert-danger alert-sm" role="alert">
                            <?php echo e($error); ?>

                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <form method="post" action="/jurnal/<?php echo e($jurnal->id); ?>">
                    <?php echo method_field('put'); ?>
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                      <label for="Waktu">Waktu</label>
                      <input type="date" class="form-control" id="Waktu" placeholder="masukkan waktu" name="wkt_jurnal" value="<?php echo e($jurnal->wkt_jurnal); ?>">
                    </div>
                    <div class="form-group">
                      <label for="Keterangan">Keterangan</label>
                      <input type="text" class="form-control" id="Keterangan" placeholder="Masukkan keterangan" name="keterangan" value="<?php echo e($jurnal->keterangan); ?>">
                    </div>
                    <div class="form-group">
                        <label for="total">total</label>
                        <input type="text" class="form-control" id="total" placeholder="Masukkan total" name="total" value="<?php echo e($jurnal->total); ?>">
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm prepend">Update</button>
                    <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                </form>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Q:\Laravel\coba2\resources\views/jurnal/edit.blade.php ENDPATH**/ ?>