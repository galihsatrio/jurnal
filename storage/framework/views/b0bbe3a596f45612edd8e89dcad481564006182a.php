<?php $__env->startSection('title','Update'); ?>


<?php $__env->startSection('container'); ?>
    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Update</h3>
                <form method="post" action="/item/<?php echo e($rekening->id); ?>">
                    <?php echo method_field('put'); ?>
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label for="jurnal_id">Keterangan</label>
                        <select name="keterangan" id="jurnal_id" class="form-control" disabled>
                            <option value="<?php echo e($jurnal->id); ?>"><?php echo e($jurnal->id); ?>. <?php echo e($jurnal->keterangan); ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama">nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama" value="<?php echo e($rekening->nama); ?>">
                    </div>
                    <div class="form-group">
                      <label for="saldo">saldo</label>
                      <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo" value="<?php echo e($rekening->saldo); ?>">
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm Sprepend">Simpan</button>
                    <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                </form>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Q:\Laravel\coba2\resources\views/jurnal/item/edit.blade.php ENDPATH**/ ?>