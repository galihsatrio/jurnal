@extends('layout.master')

@section('title','Rekening')

@section('search')
<a href="/rekening" class="badge badge-dark mr-1">Show All</a>
<form class="form-inline my-2 my-lg-0" action="/rekening" method="get">
    <input class="form-control sm-2" type="search" placeholder="cari nama.." aria-label="Cari" name="search">
    <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
      <div class="dropdown show">
        <a class="btn btn-default ml-3 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{-- {{ Auth::user()->name }} --}}
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="/logout">Log Out</a>
        </div>
      </div>
</form>
@endsection

@section('container')
    <div class="container">
        <div class="row">

                <div class="col-6 my-3">
                    <h2>Rekening</h2>
                </div>

                @if(count($errors) > 0 )
                <div class="alert alert-danger col-12">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-10">

                <button type="button" class="btn btn-primary float-right mb-4 btn-sm" data-toggle="modal" data-target="#TambahData">
                    Add Rekening
                </button>

                <table class="table table-bordered table-hover table-default">
                    <thead class="thead thead-dark">
                        <tr>
                            <th width="10px">No</th>
                            <th class="text-center">Keterangan</th>
                            <th class="text-center" width="450px">Nama</th>
                            <th class="text-center" width="170px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rekening as $no => $rekenings)
                        <tr>
                            <td class="text-center">{{$no + $rekening->firstitem()}}</td>
                            <td class="text-center">{{$rekenings->keterangan}}</td>
                            <td class="text-center">{{$rekenings->nama}}</td>
                            <td class="text-center">
                                <a href="/rekening/{{$rekenings->id}}/edit" class="btn btn-success btn-sm my-1 prepend">edit</a>
                                <form class="d-inline" action="/rekening/{{$rekenings->id}}" method="post">
                                  @method('delete')
                                  @csrf
                                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda ingin Menghapusnya')">delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-6">
                Halaman : {{ $rekening->currentPage() }} <br>
                Jumlah Data : {{ $rekening->total() }} <br><br>
                {{$rekening->links()}}
            </div>


        </div>
    </div>

  {{-- Modal Tambah Data --}}
  <div class="modal fade" id="TambahData" tabindex="-1" role="dialog" aria-labelledby="TambahDataLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="TambahData">Tambah Rekening</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            {{-- form tambah Data --}}
            <div class="container">
                <form method="post" action="/rekening/store" class="form-group">
                    @csrf
                    <div class="form-group">
                        <label for="Jurnal_id">Keterangan</label>
                        <select name="jurnal_id" id="Jurnal_id" class="form-control" required>
                            <option value="">-  Pilih -</option>
                            @foreach($jurnal as $jurnal)
                            <option value="{{$jurnal->id}}">{{$jurnal->id}}. {{$jurnal->keterangan}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama" required>
                    </div>
                    <div class="form-group">
                      <label for="saldo">Saldo</label>
                      <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo" required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-sm prepend">Selesai</button>
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kembali</button>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Modal --}}
@endsection
