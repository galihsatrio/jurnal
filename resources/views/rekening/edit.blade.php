@extends('layout.master')

@section('title','Tambah Data')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Edit Rekening</h3>

                @if(count($errors) > 0 )
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="container">
                    {{-- @foreach($rekening as $rekenings) --}}
                    <form method="post" action="/rekening/{{$rekenings->id}}" class="form-group">
                    {{-- @endforeach --}}
                        @method('put')
                        @csrf
                        <div class="form-group">
                            <label for="Jurnal_id">id Jurnal</label>
                            <select name="jurnal_id" id="Jurnal_id" class="form-control" disabled>
                                <option value="{{$jurnal->id}}"> {{$jurnal->id}}. {{$jurnal->keterangan}} </option>
                            </select>
                        </div>
                        {{-- @foreach($rekening as $rekenings) --}}
                        <div class="form-group">
                          <label for="nama">Nama</label>
                          <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama" value="{{$rekenings->nama}}">
                        </div>
                        <div class="form-group">
                          <label for="saldo">Saldo</label>
                          <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo" value="{{$rekenings->saldo}}">
                        </div>
                        {{-- @endforeach --}}
                        <button type="submit" class="btn btn-primary btn-sm prepend">Edit</button>
                        <a href="/rekening" class="btn btn-warning btn-sm">Kembali</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
