<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Masuk</title>
  </head>
  <body class="text-center bg-light">
      <div class="container">
            <div class="row align-items-start">
            </div> <br><br><br>
          <div class="row align-items-center">
              <div class="col-4">
                {{-- untuk megisi col kiri --}}
              </div>
               <span class="border border-dark rounded-lg bg-default col-4" name="tengah" id="tengah">
                <h1 class="my-3">Masuk</h1>
                <form class="form-signin text-center" method="post" action="/postlogin">
                    @csrf
                    <div class="form-group">
                      <label for="Email" class="float-left">Email :</label>
                      <input type="text" class="form-control text-center border-dark" id="Email"  name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password" class="float-left">Password :</label>
                        <input type="password" class="form-control text-center border-dark" id="password" name="password" required>
                      </div>

                    {{-- <a href="">Lupa password</a> --}}
                    <p>Belum punya akun? <a href="/register">Daftar</a></p>
                    <button class="btn btn-outline-dark my-3" type="submit">Masuk</button>
                </form>
               </span>
          </div>
        </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
