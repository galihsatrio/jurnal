@extends('layout.master')

@section('title','Buku Besar')

@section('search')
<a href="/bukubesar" class="badge badge-light">Show All</a>
<form class="form-inline my-2 my-lg-0" action="/bukubesar" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Cari" aria-label="Cari" name="search">
    <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>
</form>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">


                <h2 class="my-4">Buku Besar</h2>

                <table class="table table-hover table-striped table-light table-bordered">
                    <thead class="thead thead-dark">
                        <tr>
                            <th width="50px">No</th>
                            <th class="text-center" width="120px">Waktu</th>
                            <th class="text-center">Keterangan</th>
                            <th witdh="150px" class="text-center">Saldo</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jurnal as $no => $jurnals)
                        <tr>
                            <td>{{$no + $jurnal->firstItem()}}</td>
                            <td class="text-center">
                                <p class="font-italic">
                                    {{ \Carbon\Carbon::parse($jurnals->wkt_jurnal)->format('d, M Y') }} <br>

                                </p>
                                <p class="font-italic font-weight-light">
                                    {{ \Carbon\Carbon::parse($jurnals->wkt_jurnal)->diffForHumans() }}
                                </p>
                            </td>
                            <td class="text-center">{{$jurnals->keterangan}}</td>
                            <td>
                                <ul>
                                    @foreach($items[$jurnals->id]['rekening'] as $item)
                                    <li>{{$item->saldo}}</li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>
                                {{$items[$jurnals->id]['total']}}
                            </td>
                        @endforeach
                    </tbody>
                </table>

                    Halaman : {{$jurnal->currentPage()}} <br>
                    Jumlah Data : {{$jurnal->total()}} <br><br><br>


                {{$jurnal->links()}}
            </div>
        </div>
    </div>
@endsection
