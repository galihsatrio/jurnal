@extends('layout.master')

@section('title','Update')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Update</h3>
                <form method="post" action="/item/{{$rekening->id}}">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label for="jurnal_id">Keterangan</label>
                        <select name="keterangan" id="jurnal_id" class="form-control" disabled>
                            <option value="{{$jurnal->id}}">{{$jurnal->id}}. {{$jurnal->keterangan}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama">nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama" value="{{$rekening->nama}}">
                    </div>
                    <div class="form-group">
                      <label for="saldo">saldo</label>
                      <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo" value="{{$rekening->saldo}}">
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm Sprepend">Simpan</button>
                    <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                </form>

            </div>
        </div>
    </div>
@endsection
