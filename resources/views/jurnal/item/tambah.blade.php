@extends('layout.master')

@section('title','Tambah Data')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Tambah Data</h3>

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="container">
                    <form method="post" action="/jurnaltambahitem">
                        @csrf
                        <div class="form-group">
                            <label for="Jurnal_id">Keterangan</label>
                            <select name="jurnal_id" id="Jurnal_id" class="form-control" required>
                                <option value="">-  Pilih -</option>
                                @foreach($jurnal as $jurnals)
                                <option value="{{$jurnals->id}}">{{$jurnals->id}}. {{$jurnals->keterangan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="nama">Nama</label>
                          <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama">
                        </div>
                        <div class="form-group">
                          <label for="saldo">Saldo</label>
                          <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo">
                        </div>

                        <button type="submit" class="btn btn-primary btn-sm prepend">Selesai</button>
                        <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
