@extends('layout.master')

@section('title','Update')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Update</h3>
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger alert-sm" role="alert">
                            {{$error}}
                        </div>
                    @endforeach
                @endif
                <form method="post" action="/jurnal/{{$jurnal->id}}">
                    @method('put')
                    @csrf
                    <div class="form-group">
                      <label for="Waktu">Waktu</label>
                      <input type="date" class="form-control" id="Waktu" placeholder="masukkan waktu" name="wkt_jurnal" value="{{$jurnal->wkt_jurnal}}">
                    </div>
                    <div class="form-group">
                      <label for="Keterangan">Keterangan</label>
                      <input type="text" class="form-control" id="Keterangan" placeholder="Masukkan keterangan" name="keterangan" value="{{$jurnal->keterangan}}">
                    </div>
                    <div class="form-group">
                        <label for="total">total</label>
                        <input type="text" class="form-control" id="total" placeholder="Masukkan total" name="total" value="{{$jurnal->total}}">
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm prepend">Update</button>
                    <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                </form>

            </div>
        </div>
    </div>
@endsection
