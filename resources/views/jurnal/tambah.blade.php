@extends('layout.master')

@section('title','Tambah Data')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
                <h3>Tambah Data</h3>

                @if (count($errors)>0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-sm" role="alert">
                        {{$error}}
                    </div>
                    @endforeach
                @endif
                <form method="post" action="/jurnaltambah">
                    @csrf
                    <div class="form-group">
                      <label for="wkt_jurnal">Waktu</label>
                      <input type="date" class="form-control" id="wkt_jurnal" placeholder="masukkan waktu" name="wkt_jurnal">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" placeholder="Masukkan keterangan" name="keterangan">
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm prepend">Selesai</button>
                    <a href="/jurnal" class="btn btn-warning btn-sm">Kembali</a>
                </form>

            </div>
        </div>
    </div>
@endsection
