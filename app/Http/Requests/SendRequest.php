<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jurnal_id' => 'required',
            'name' => 'required|min:3',
            'saldo' => 'required|numeric',
            // 'keterangan' => 'required|min:3',
            // 'total' => 'required|numeric'
        ];
    }
}
