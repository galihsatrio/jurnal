<?php

namespace App\Http\Controllers;

use App\Rekening;
use App\Jurnal;
use App\Http\Requests\SendRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if($request->has('search')){
            $search = $request->get('search');
            $rekening = DB::table('rekenings')->join('jurnals','rekenings.jurnal_id','=','jurnals.id')->select('rekenings.*','jurnals.keterangan')->where('nama','like',"%".$search."%")->paginate();
        }else{
            $rekening = DB::table('rekenings')->join('jurnals','rekenings.jurnal_id','=','jurnals.id')->select('rekenings.*','jurnals.keterangan')->paginate(10);
        }

        $jurnal = DB::table('jurnals')->get();
        return view('rekening.rekening', compact('rekening','jurnal'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->all();

        // $this->validate($request, [
        //     'nama'=> 'required|min:3',
        //     'saldo'=> 'required|numeric'
        // ]);

        DB::table('rekenings')->insert([
            'jurnal_id'=> $request->jurnal_id,
            'nama'=> $request->nama,
            'saldo'=> $request->saldo
        ]);
        return redirect('/rekening');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rekenings = DB::table('rekenings')->where('id', $id)->first();
        $jurnal = DB::table('jurnals')->where('id',  $rekenings->jurnal_id)->first();

        return view('rekening.edit', compact('rekenings','jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama'=> 'required',
            'saldo'=> 'required'
        ]);

        DB::table('rekenings')->where('id', $request->id)->update([
            'nama'=> $request->nama,
            'saldo'=> $request->saldo
        ]);
        return redirect('/rekening');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('rekenings')->where('id', $id)->delete();
        return redirect('/rekening');
    }
}
