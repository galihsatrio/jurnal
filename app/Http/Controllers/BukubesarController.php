<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Jurnal;
use App\Rekening;

class BukubesarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->has('search')){

            $search = $request->get('search');

            $items = [];

            $jurnal = DB::table('jurnals')->where('keterangan','like',"%".$search."%")->paginate();
            foreach($jurnal as $jurnals){
                $rekening = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->get();
                $total = DB::table('rekenings')->where('jurnal_id',$jurnals->id)->sum('saldo');

                $items[$jurnals->id] = [
                    'rekening' => $rekening,
                    'total' => $total
                ];
            }

        }else{

            $items = [];

            $jurnal = DB::table('jurnals')->paginate(5);
            foreach($jurnal as $jurnals){
                $rekening = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->get();
                $total = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->sum('saldo');

                $items[$jurnals->id] = [
                    'rekening' => $rekening,
                    'total' => $total
                ];

            }
        }

        // $jurnal = Jurnal::with('rekenings')->paginate(10);
        return view('bukubesar.bukubesar', compact('jurnal','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
