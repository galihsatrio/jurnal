<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Rekening;
use App\Jurnal;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        $jurnal = DB::table('jurnals')->get();
        return view('jurnal.item.tambah', compact('jurnal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'jurnal_id' => 'required',
            'nama' => 'required|min:3',
            'saldo' => 'required|numeric'
        ]);

        DB::table('rekenings')->insert([
            'jurnal_id'=>$request->jurnal_id,
            'nama'=>$request->nama,
            'saldo'=>$request->saldo
        ]);

        return redirect('/jurnal')->with('rekening_success','Tambah data item berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rekening = DB::table('rekenings')->where('id', $id)->first();
        $jurnal = DB::table('jurnals')->where('id', $rekening->jurnal_id)->first();
        return view('jurnal.item.edit', compact('rekening','jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|min:3',
            'saldo' => 'required|numeric'
        ]);

        Rekening::where('id', $id)->update([
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);

        return redirect('/jurnal')->with('success_update_item','Update jurnal item berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rekening::destroy($id);
        return redirect('/jurnal');
    }
}
