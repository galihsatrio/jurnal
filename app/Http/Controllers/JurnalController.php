<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illumiate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Jurnal;
use \App\Rekening;

class JurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if($request->has('search'))
        {

            $search = $request->get('search');
            $jurnal = DB::table('jurnals')->where('keterangan','like','%'.$search."%")->paginate();
            foreach($jurnal as $jurnals){
                $rekening = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->get();
                $total = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->sum('saldo');

                $items[$jurnals->id] =[
                    'rekening' => $rekening,
                    'total' => $total
                ];

            }

        }else{

            $items = [];

            $jurnal = DB::table('jurnals')->paginate(5);
            foreach($jurnal as $jurnals){
                $rekening = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->get();
                $total = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->sum('saldo');

                $items[$jurnals->id] =[
                    'rekening' => $rekening,
                    'total' => $total
                ];

            }
        }

        $total = DB::table('rekenings')->sum('saldo');
        return view('jurnal.jurnal', compact('jurnal','items','total'));

    }



    public function create()
    {
        $jurnal = DB::table('jurnal')->get();
        return view('jurnal.tambah', compact('jurnal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'wkt_jurnal' => 'required',
            'keterangan' => 'required|max:30|min:3',
        ]);

        Jurnal::create([
            'wkt_jurnal' => $request->wkt_jurnal,
            'keterangan' => $request->keterangan,
            'total' => $request->total
        ]);

        return redirect()->back()->with('jurnal_success','Tambah Data Jurnal Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param App\Jurnal $jurnal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurnal = DB::table('jurnals')->where('id', $id)->first();
        return view('jurnal.edit', compact('jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurnal $jurnal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'wkt_jurnal' => 'required',
            'keterangan' => 'required',
            'total' => 'required|numeric'
        ]);

        Jurnal::where('id', $id)
            ->update([
                'wkt_jurnal'=>$request->wkt_jurnal,
                'keterangan'=>$request->keterangan,
                'total' => $request->total

        ]);

        return redirect('/jurnal')->with('success_update_jurnal','Update jurnal berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jurnal::destroy($id);
        return redirect('/jurnal');
    }
}
