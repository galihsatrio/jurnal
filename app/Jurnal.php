<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $table = 'jurnals';

    protected $fillable = [
        'wkt_jurnal',
        'keterangan',
        'created_at',
        'update_at'
    ];

    // protected $dates = [
    //     'wkt_jurnal',
    //     'created_at',
    //     'update_at'
    // ];

    public function rekenings(){
        return $this->hasMany('App\Rekening');
    }


}
