<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware'=>'auth'], function(){

    Route::get('/home', function () {
        return view('welcome');
    })->middleware('auth');

    //rekening
    Route::get('/rekening', 'RekeningController@index');
    Route::post('/rekening/store','RekeningController@store');
    Route::get('/rekening/{id}/edit', 'RekeningController@edit');
    Route::put('/rekening/{id}', 'RekeningController@update');
    Route::delete('/rekening/{id}', 'RekeningController@destroy');

    //jurnal
    Route::get('/jurnal', 'JurnalController@index');
    Route::post('/jurnaltambah', 'JurnalController@store');
    Route::get('/jurnal/{id}/edit', 'JurnalController@edit');
    Route::put('/jurnal/{id}', 'JurnalController@update');
    Route::delete('/jurnal/{id}', 'JurnalController@destroy');

    //jurnal_item
    Route::get('/jurnal/tambahitem', 'ItemController@create');
    Route::post('/jurnaltambahitem', 'ItemController@store');
    Route::get('/item/{id}/edit', 'ItemController@edit');
    Route::put('/item/{id}', 'ItemController@update');
    Route::delete('/item/{id}','ItemController@destroy');

    //bukubesar
    Route::get('/bukubesar', 'BukubesarController@index');
});


//login
Route::get('/login','AuthController@Login')->name('login');
Route::post('/postlogin','AuthController@postLogin');
Route::get('/logout','AuthController@logout');

